package restaurant;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class RestraurantMenu{
	
int itemId;
String itemName;
int quantity;
double price;
Scanner input;
File file;
FileReader fileReader;
BufferedReader bufferedReader;
static int setBillSum=0;
int totalSum=0;
 int sum=0;
/*
 * Function : to displayMenu menu
 * Input	: No input required
 * Output	: Displays Menu to user
 */

public BufferedReader openfile(){
	file = new File("C:\\Users\\intimetec\\Desktop\\Resto\\MENU1.txt");
	try {
		fileReader = new FileReader(file);
	} catch (FileNotFoundException e) {
		System.out.println(e);
	}
	bufferedReader = new BufferedReader(fileReader);
	return bufferedReader ;
}

public void closefile(BufferedReader r){
	try 
    {
        if (r != null)
        {
            r.close();
        }
    }
    catch (Exception e)
    {
    	System.out.println(e);
    }
}

public void displayMenu(int a)  {
	RestraurantMenu menuobj= new RestraurantMenu();
String line = null;
bufferedReader = openfile();
System.out.println("Item Id\tItem name\tPrice");
		try {
			while ((line = bufferedReader.readLine()) != null) 
			{
				String menu []= line.split("\t"); 
				if(a==3)
				{
				System.out.println(menu[0]+"\t"+menu[1]+"\t\t"+menu[2]+"\t\t"+menu[3]);
				}
				else
					System.out.println(menu[0]+"\t"+menu[1]+"\t\t"+menu[2]);
					
			}
			bufferedReader.close();
		} catch (IOException e) {
			System.out.println(e);
		}
		int goback = 0;
		if(a==1){
		do {
			System.out.println("Press 1 to go back\n");
			input = new Scanner(System.in);
			goback = input.nextInt();
		} while (goback != 1);
		}
	
menuobj.closefile(bufferedReader);
}//End of displayMenu Method

public void writeintofile(Map<String,String> newmap) throws IOException{
	File writeIntoFile = new File("C:\\Users\\intimetec\\Desktop\\Resto\\MENU1.txt");
	FileWriter fileWriter = new FileWriter(writeIntoFile);
	BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		for (Map.Entry<String, String> entry : newmap.entrySet()) 
		{
			bufferedWriter.write(entry.getValue());
			bufferedWriter.append(System.lineSeparator());
		}
	bufferedWriter.close();
}
}//End of class

class RestroOrder extends RestraurantMenu{
static int totalSum=0;
static	Map<Integer,Integer> orderMap ;
boolean flag=false;
/*
 * Function : to take user order and store it in map
 * Input	: No input required
 * Output	: Stores User Order into map
 */
public void order() 
{
	flag=false;
	RestroInventory inventoryobject = new RestroInventory();
		 orderMap = new HashMap<Integer,Integer>();

int choice = 0;
		do {
			System.out.println("\n Enter the Item id");
			input = new Scanner(System.in);
			itemId = input.nextInt();
			System.out.println("\n Enter the Quantity");
			quantity = input.nextInt();
		if(quantity>0)	
		{
			int flag=0;
			try {
				flag=inventoryobject.stockCheck(itemId,quantity);
			} catch (IOException e) {
				System.out.println(e+"\n Correct numerical value");
			}
			if(flag!=1)
			{
					if(orderMap.containsKey(itemId))
					{
						orderMap.put(itemId, orderMap.get(itemId)+quantity);
					}
					else
					{
						orderMap.put(itemId, quantity);
					}
			}
			else
			{
				System.out.print("out of stock");
			}
		}
		else
		{
			System.out.println("Invalid quantity. Enter quantity greater that 0 ");
		}
			System.out.println("\n Enter 0 to return or any key to continue your order");
			choice = input.nextInt();
		} while (choice != 0);

flag=true;
}//End of order method

/*
 * Function : to return map that has user order
 * Input	: No input required
 * Output	: Returns Map
 */	
protected Map<Integer,Integer> getMap()
{
		return orderMap;
}//End of Map method

/*
 * Function : to calculate total amount of order 
 * Input	: No input required
 * Output	: Calculates total amount to be paid and stores it in "setSum" variable
 */
public void bill() throws IOException 
{
//int sum=0;
String line=null;

bufferedReader = openfile();
	while((line=bufferedReader.readLine())!=null)
	{
	String words[]=line.split("\t");
		if(orderMap.containsKey(Integer.parseInt(words[0])))
		{
			//quantity* price
		sum+=orderMap.get(Integer.parseInt(words[0]))*Integer.parseInt(words[2]);
		}
	}
//	totalSum+=sum;
System.out.println("Sum "+sum);
setSum(sum);
closefile(bufferedReader);
flag=false;
}//End of Bill method

/*
 * Function : To Set sum
 * Input	: Takes input from bill method
 * Output	: Stores in static variable
 */
private void setSum(int sum)
{
setBillSum=sum;
}
/*
 * Function : to get sum 
 * Input	: No input required
 * Output	: Returns total amount to be paid
 */	
protected int getSum() 
{
		return setBillSum;
}

protected void setTotal(int total) 
{
totalSum+=total	;
}

public int getTotal() 
{
		return totalSum;
}
}//End of RestroOrder class

class RestroInventory {
	
/*
 * Function : to updateInventory the stock
 * Input    : No input required
 * Output   : Stock details contained in file will be updated
 */
	
	public int stockCheck(int itemId,int quantity) throws IOException{
		RestraurantMenu menuobj= new RestraurantMenu();
		BufferedReader bufferedReader = menuobj.openfile();
		int flag=0;
		String line=null;
		while((line=bufferedReader.readLine())!=null)
		{
			String words[] = line.split("\t");
			if(itemId == Integer.parseInt(words[0]) )
			{
				if(quantity>=Integer.parseInt(words[3]))
				{
					System.out.println("entered quantity more than available \n"
							+ "enter quantity less than "+Integer.parseInt(words[3]));
					flag = 1;
					return flag;
				}
			}
			else
				flag=0;
			return flag;
		}
		System.out.println("yasser "+flag);
		return flag;
	}
	public void updateInventory() throws NumberFormatException, IOException 
	{
	RestroOrder o = new RestroOrder();
	Map<Integer,Integer> map=	o.getMap();
//	Map<Integer,Integer> map=	orderMap;
	RestraurantMenu menuobj  = new RestraurantMenu();
	
	Map<String,String> newmap=	new HashMap<String,String>();
	BufferedReader bufferedReader =menuobj.openfile();
String line=null;	
		while ((line = bufferedReader.readLine()) != null) 
		{
			String words[] = line.split("\t");
				if(map.containsKey(Integer.parseInt(words[0])))
				{
					int qty =Integer.parseInt(words[3])- map.get(Integer.parseInt(words[0]));
					//store matched items into map
					newmap.put(words[0],words[0]+"\t"+words[1]+"\t"+words[2]+"\t"+Integer.toString(qty));
					System.out.println("ordered "+ words[1]+" quantity "+ map.get(Integer.parseInt(words[0])));
				}
				else
				{
					//store not matched items into map
					newmap.put(words[0], words[0]+"\t"+words[1]+"\t"+words[2]+"\t"+words[3]);
				}
		}	
	//Write Updated stock information into file
	menuobj.writeintofile(newmap);
	}//End of order Method

	public 	void addMenuItem() throws IOException{
		Scanner input = new Scanner(System.in);
	Map<String , String> additem = new HashMap<String,String>();
	RestraurantMenu rm = new RestraurantMenu(); 
	BufferedReader br= rm.openfile();
	String line=null;
	while((line= br.readLine())!=null){
		String words[] = line.split("\t");
		additem.put(words[0],words[0]+"\t"+words[1]+"\t"+words[2]+"\t"+words[3]);
		
	}
	System.out.println("Enter item ID");
	int id = input.nextInt();
	System.out.println("Enter item Name");
	String name = input.next();
	System.out.println("Enter item Price");
	int price = input.nextInt();
	System.out.println("Enter item Quantity");
	int quantity = input.nextInt();
	additem.put(Integer.toString(id),Integer.toString(id)+"\t"+name+"\t"+Integer.toString(price)+"\t"+Integer.toString(quantity) );
	rm.writeintofile(additem);
	//System.out.println(id + name +price+ quantity);
	}	
}//End of class RestroInventory

class Manager extends RestroInventory {
//class Manager {	
	
	public void manager() throws IOException{
		OneMoreRestaurant	obj = new OneMoreRestaurant();
		while(true)
		{
			System.out.println("1. Find Total buisness\n2. Get Stock\n3. Add Item to the menu\n4. Go back\n");
			obj.input = new Scanner(System.in);
			int choice1 = obj.input.nextInt();
			switch(choice1){
			case 1 : RestroOrder obj2 = new RestroOrder();
					 System.out.println("Todays total buisness "+obj2.getTotal());break;
			
			case 2 : RestraurantMenu menuobj = new RestraurantMenu();
					 menuobj.displayMenu(3);break;
					
			case 3 : addMenuItem();// RestroInventory class function
					break;
				
			case 4 : return;
				
			default: System.out.println("Invalid choice . Enter Again\n");
			break;
			}
		}
	}
}
public class OneMoreRestaurant 
{
Scanner input;

public void customeroption()
{
	RestraurantMenu restroMenuobject = new RestraurantMenu();
	RestroOrder restroOrderobject = new RestroOrder();
	while(true)
	{
		System.out.println("\n1\tMenu");
		System.out.println("2\tOrder");
		System.out.println("3\tBill");
		System.out.println("4\tGenerate Final bill and Exit");

		restroMenuobject.input = new Scanner(System.in);
		int ch = restroMenuobject.input.nextInt();
		try{
				switch (ch) 
				{
					case 1: restroMenuobject.displayMenu(1);
							break;
							
					case 2: restroOrderobject.displayMenu(2);
							restroOrderobject.order();
							break;
							
					case 3: if(restroOrderobject.flag!=false){
						    restroOrderobject.bill();
						    }
							else
							{
								System.out.println("Order first");
							}
							break;
							
					case 4: RestroInventory restroInventoryobject = new RestroInventory();
							restroInventoryobject.updateInventory();
							System.out.println("\n\nFinal bill is " + restroOrderobject.getSum() + " "
									+ "Rupees.\n Thankyou. Come Again");
							restroOrderobject.setTotal(restroOrderobject.getSum());
							return;
							
					default: System.out.println("Invalid choice . Enter Again\n");
							break;
				}
			}catch(Exception e){
				System.out.println(e);
			}
	}
}

public static void main(String[]args) throws IOException
{
		OneMoreRestaurant	obj = new OneMoreRestaurant();
		Manager obj1= new Manager();
		while(true)
		{
			System.out.println("\n1. Manager\n2. Customer\n");
			obj.input = new Scanner(System.in);
			int choice = obj.input.nextInt();
			if(choice==1)
			{
				obj1.manager();
			}
			else if(choice==2)
			{
				obj.customeroption();
			}
			else
				System.out.println("Invalid option\n");
		}	
}
}
